# Lekcja 2
## Zadanie 2 - basic 
Stwórz funkcję `pow`, która będzie wykonywała działanie potęgowania. Funkcja będzie przyjmowała dwa argumenty typu `number`. Pierwszym parametrem będzie podstawa potęgi, a drugim wykładnik. Funkcja powinna zwrócić wykonaną operację. 

Przykładowy input:
`pow(2, 2);`

Przykładowy output:
`4`
